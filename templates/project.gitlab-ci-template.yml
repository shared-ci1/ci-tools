spec:
  inputs:
    PROJECT_NAME:
    DOCKER_ENABLED:
      default: "false"
    DOCKER_FILE_PATH:
      default: "Dockerfile"
    DOCKER_CONTEXT_PATH:
      default: "."
    APP_PRODUCTION_IMAGE_PREFIX:
      default: "${DOCKER_REGISTRY_URL}/${CI_PROJECT_PATH}"
    APP_IMAGE_POSTFIX:
      default: ""
    APP_IMAGE:
      default: "${CI_REGISTRY_IMAGE}:${CI_PIPELINE_ID}"
    DOCKER_PUBLISH_LATEST:
      default: "false"
    HELM_ENABLED:
      default: "false"
    HELM_CHART_PATH:
      default: "./helm/${PROJECT_NAME}"
    HELM_LINT_FLAGS:
      default: ""
    HELM_OUTPUT_DIR:
      default: "helm-release/${PROJECT_NAME}"
    DEPLOY_TO_FRONTENDTESTS_ENABLED:
      default: "false"
    DEPLOY_TO_PLAN_ENABLED:
      default: "false"
    DEPLOY_NAMESPACE:
      default: ""
    DEPLOY_RELEASE_NAME:
      default: ""
---

'.variables-$[[ inputs.PROJECT_NAME ]]':
  variables:
    DOCKER_REGISTRY_URL: "${GLOBAL_ARTIFACTORY_DOCKER_URL}"
    PROJECT_NAME: "$[[ inputs.PROJECT_NAME ]]"
    DOCKER_ENABLED: "$[[ inputs.DOCKER_ENABLED ]]"
    DOCKER_FILE_PATH: "$[[ inputs.DOCKER_FILE_PATH ]]"
    DOCKER_CONTEXT_PATH: "$[[ inputs.DOCKER_CONTEXT_PATH ]]"
    APP_PRODUCTION_IMAGE_PREFIX: "$[[ inputs.APP_PRODUCTION_IMAGE_PREFIX ]]"
    APP_IMAGE_POSTFIX: "$[[ inputs.APP_IMAGE_POSTFIX ]]"
    APP_IMAGE: "$[[ inputs.APP_IMAGE ]]"
    DOCKER_PUBLISH_LATEST: "$[[ inputs.DOCKER_PUBLISH_LATEST ]]"
    HELM_ENABLED: "$[[ inputs.HELM_ENABLED ]]"
    HELM_CHART_PATH: "$[[ inputs.HELM_CHART_PATH ]]"
    HELM_LINT_FLAGS: "$[[ inputs.HELM_LINT_FLAGS ]]"
    HELM_OUTPUT_DIR: "$[[ inputs.HELM_OUTPUT_DIR ]]"
    DEPLOY_TO_FRONTENDTESTS_ENABLED: "$[[ inputs.DEPLOY_TO_FRONTENDTESTS_ENABLED ]]"
    DEPLOY_TO_PLAN_ENABLED: "$[[ inputs.DEPLOY_TO_PLAN_ENABLED ]]"
    DEPLOY_NAMESPACE: "$[[ inputs.DEPLOY_NAMESPACE ]]"
    DEPLOY_RELEASE_NAME: "$[[ inputs.DEPLOY_RELEASE_NAME ]]"

stages:
  - global
  - lint
  - prepare
  - build
  - test
  - package
  - integration-test
  - publish
  - deploy
  - release

lint:helm:tools:app:$[[ inputs.PROJECT_NAME ]]:
  stage: lint
  image: alpine
  extends:
    - '.variables-$[[ inputs.PROJECT_NAME ]]'
  rules:
    - if: $HELM_ENABLED == "true"
  script:
    - echo "helm-lint ${HELM_CHART_PATH} ${HELM_LINT_FLAGS}"

lint:helm:old-k8s-api:app:$[[ inputs.PROJECT_NAME ]]:
  stage: lint
  image: alpine
  extends:
    - '.variables-$[[ inputs.PROJECT_NAME ]]'
  rules:
    - if: $HELM_ENABLED == "true"
  variables:
    K8S_TARGET_VERSION: v1.9.0
  script:
    - echo "requirements.yaml >> ${HELM_CHART_PATH}/.helmignore"
    - echo "helm template ${HELM_CHART_PATH} --api-versions ${K8S_TARGET_VERSION} | kubepug --k8s-version ${K8S_TARGET_VERSION} --error-on-deleted --error-on-deprecated --input-file=-"
  allow_failure: true

lint:helm:new-k8s-api:app:$[[ inputs.PROJECT_NAME ]]:
  stage: lint
  image: alpine
  extends:
    - '.variables-$[[ inputs.PROJECT_NAME ]]'
  rules:
    - if: $HELM_ENABLED == "true"
  variables:
    K8S_TARGET_VERSION: v1.22.0
  script:
    - echo "requirements.yaml >> ${HELM_CHART_PATH}/.helmignore"
    - echo "helm template ${HELM_CHART_PATH} --api-versions ${K8S_TARGET_VERSION} | kubepug --k8s-version ${K8S_TARGET_VERSION} --error-on-deleted --error-on-deprecated --input-file=-"
  allow_failure: true

lint:docker:lint:app:$[[ inputs.PROJECT_NAME ]]:
  stage: lint
  image: alpine
  extends:
    - '.variables-$[[ inputs.PROJECT_NAME ]]'
  rules:
    - if: $DOCKER_ENABLED == "true"
  variables:
    HADOLINT_OVERRIDE_INFO: DL3008,DL3013,DL3018,DL3033,DL3041
    HADOLINT_FAILURE_THRESHOLD: warning
  script:
    - echo "To override lint config add inline ignore (https://github.com/hadolint/hadolint#inline-ignores) or create .hadolint.yaml file (https://github.com/hadolint/hadolint#configure)"
    - echo "hadolint ${DOCKER_FILE_PATH}"

package-docker:app:$[[ inputs.PROJECT_NAME ]]:
  stage: package
  image: alpine
  extends:
    - '.variables-$[[ inputs.PROJECT_NAME ]]'
  rules:
    - if: $DOCKER_ENABLED == "true"
  before_script:
    - echo "sh ${SCRIPTS_DIR}/scripts/shared/docker-login-to-gitlab.sh"
  script:
    - echo "sh ${SCRIPTS_DIR}/scripts/shared/package/docker-package.sh ${DOCKER_CONTEXT_PATH} ${APP_IMAGE}${APP_IMAGE_POSTFIX} ${DOCKER_FILE_PATH}"

package-helm:app:$[[ inputs.PROJECT_NAME ]]:
  stage: package
  image: alpine
  extends:
    - '.variables-$[[ inputs.PROJECT_NAME ]]'
  rules:
    - if: $HELM_ENABLED == "true"
  before_script:
    - echo "APP_VERSION=$(cat ${VERSION_OUTPUT_PATH} | sed 's/\//_/g')"
  script:
    - echo "helm-package ${HELM_CHART_PATH} ${APP_VERSION} ${HELM_OUTPUT_DIR}"
  artifacts:
    expire_in: 1 days
    paths:
      - "${HELM_OUTPUT_DIR}"

publish-docker:app:$[[ inputs.PROJECT_NAME ]]:
  stage: publish
  image: alpine
  extends:
    - '.variables-$[[ inputs.PROJECT_NAME ]]'
  rules:
    - if: '$DOCKER_ENABLED == "true"'
      when: manual
  before_script:
    - echo "sh ${SCRIPTS_DIR}/scripts/shared/docker-login.sh"
    - echo "sh ${SCRIPTS_DIR}/scripts/shared/docker-login-to-gitlab.sh"
    - echo "if [ -f ${VERSION_OUTPUT_PATH} ]; then APP_VERSION=$(cat ${VERSION_OUTPUT_PATH}) ;elif [ ${CI_COMMIT_REF_NAME} == ${CI_DEFAULT_BRANCH} ] && [ -z ${CI_COMMIT_TAG} ]; then APP_VERSION=latest; elif [ ! -z ${CI_COMMIT_TAG} ]; then APP_VERSION=${CI_COMMIT_TAG}; else APP_VERSION=${CI_COMMIT_REF_SLUG}; fi"
    - echo "APP_PRODUCTION_IMAGE=${APP_PRODUCTION_IMAGE_PREFIX}${APP_IMAGE_POSTFIX}:${APP_VERSION}"
    - echo "if ( [ ! -z ${CI_COMMIT_TAG} ] || [ -f ${VERSION_OUTPUT_PATH} ] ) && [ ${CI_COMMIT_BRANCH} == ${CI_DEFAULT_BRANCH} ]; then LATEST_PRODUCTION_IMAGE=${APP_PRODUCTION_IMAGE_PREFIX}:latest; fi"
  script:
    - echo "sh ${SCRIPTS_DIR}/scripts/shared/publish/publish-docker-app.sh ${APP_IMAGE} ${APP_PRODUCTION_IMAGE}"
    - echo "if [ ${DOCKER_PUBLISH_LATEST} == 'true' ] && [ ! -z ${LATEST_PRODUCTION_IMAGE} ] ; then sh ${SCRIPTS_DIR}/scripts/shared/publish/publish-docker-app.sh ${APP_IMAGE} ${LATEST_PRODUCTION_IMAGE}; fi"

publish-helm:app:$[[ inputs.PROJECT_NAME ]]:
  stage: publish
  image: alpine
  extends:
    - '.variables-$[[ inputs.PROJECT_NAME ]]'
  rules:
    - if: '$HELM_ENABLED == "true"'
      when: manual
  script:
    - echo "helm-publish ${HELM_OUTPUT_DIR} ${GLOBAL_ARTIFACTORY_HELM_URL} ${GLOBAL_ARTIFACTORY_HELM_USERNAME} ${GLOBAL_ARTIFACTORY_HELM_PASSWORD}"

'.deploy_job_template-$[[ inputs.PROJECT_NAME ]]':
  stage: deploy
  image: alpine
  extends:
    - '.variables-$[[ inputs.PROJECT_NAME ]]'
  environment:
    name: ${PROJECT_NAME}-${CLUSTER_NAME}
  retry: 1
  variables:
    GIT_STRATEGY: none
    CONFIGS_SERVICE_URL: ${GLOBAL_STAGING_CONFIGS_SERVICE_URL}
  before_script:
    - echo "APP_VERSION=`cat ${VERSION_OUTPUT_PATH}`"
  script:
    - echo "deploy-release
      --auth-service-url ${GLOBAL_INFRA_AUTH_SERVICE_URL}
      --auth-service-client-id ${GLOBAL_AUTH_SERVICE_CLIENT_ID}
      --auth-service-client-secret ${GLOBAL_AUTH_SERVICE_CLIENT_SECRET}
      --configs-service-url ${CONFIGS_SERVICE_URL}
      --charts-service-url ${GLOBAL_PRODUCTION_CHARTS_SERVICE_URL}
      --cluster ${CLUSTER_NAME}
      --namespace ${DEPLOY_NAMESPACE}
      --release ${DEPLOY_RELEASE_NAME}
      --version-to-install ${APP_VERSION}
      --force-version-update"
  when: manual

deploy:frontendtests:$[[ inputs.PROJECT_NAME ]]:
  extends:
    - '.deploy_job_template-$[[ inputs.PROJECT_NAME ]]'
  rules:
    - if: $DEPLOY_TO_FRONTENDTESTS_ENABLED == "true"
  variables:
    CLUSTER_NAME: frontendtests

deploy:plan:$[[ inputs.PROJECT_NAME ]]:
  extends:
    - '.deploy_job_template-$[[ inputs.PROJECT_NAME ]]'
  rules:
    - if: $DEPLOY_TO_PLAN_ENABLED == "true"
  variables:
    CLUSTER_NAME: plan
